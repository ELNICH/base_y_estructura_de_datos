/*=====================================================================================*/
/*---------- NAYHUA LIMA GIOVANNI ELNICH -------- ID: 001441692 -----------------------*/
/*=====================================================================================*/

-- DARLE MI RECOMENDACION QUE EJECUTE PARTE POR PARTE EL QUERY YA QUE SI LO EJECUTA DE GOLPE MANDARA ERROR // ESPERO SU COMPRENSION GRACIAS //

/* CREAMOS LA ESTA BASE DE DATOS DE ACUERDO A LA IMAGEN MOSTRADA EN EL TRABAJO FINAL */
CREATE DATABASE ENTREGABLE_2
GO

USE ENTREGABLE_2
GO

CREATE TABLE DPTO(
	DPTO_NO INT             NOT NULL PRIMARY KEY,
	DPTO_NOMBRE VARCHAR(50) NOT NULL,
	DPTO_LOC VARCHAR(50)    NOT NULL
)
GO

CREATE TABLE EMP(
	EMP_COD CHAR (3)     NOT NULL PRIMARY KEY,
	EMP_NOM VARCHAR(50)  NOT NULL,
	EMP_APE VARCHAR(50)  NOT NULL,
	EMP_OFI VARCHAR(40)  NOT NULL,
	EMP_DIRE VARCHAR(50) NOT NULL,
	EMP_FECHAALT DATE    NOT NULL,
	EMP_SALARIO MONEY    NOT NULL,
	EMP_COMISION MONEY   NULL,
	DPTO_NO INT          NOT NULL REFERENCES DPTO
)
GO

CREATE TABLE ENFERMO(
	ENF_INSCRIPCION CHAR(3)   NOT NULL,
	ENF_NOMBRE VARCHAR(50)    NOT NULL,
	ENF_APELLIDO VARCHAR(50)  NOT NULL,
	ENF_DIRECCION VARCHAR(50) NOT NULL,
	ENF_FECHANAC DATE         NOT NULL,
	ENF_ESTADOSALUD VARCHAR(50)         NOT NULL,
	ENF_NSS VARCHAR(50)       NOT NULL
)
GO


INSERT INTO DPTO (DPTO_NO, DPTO_NOMBRE, DPTO_LOC) VALUES
(1, 'Nueva York', 'Estado de Nueva York'),
(2, 'San Francisco', 'California'),
(3, 'Chicago', 'Illinois'),
(4, 'Los �ngeles', 'California'),
(5, 'Dallas', 'Texas'),
(6, 'Miami', 'Florida'),
(7, 'Seattle', 'Washington'),
(8, 'Boston', 'Massachusetts'),
(9, 'Washington, D.C.', 'Distrito de Columbia'),
(10, 'Denver', 'Colorado');


INSERT INTO EMP (EMP_COD, EMP_NOM, EMP_APE, EMP_OFI, EMP_DIRE, EMP_FECHAALT, EMP_SALARIO, EMP_COMISION, DPTO_NO) VALUES
('001', 'John',    'Doe',   'Pediatra', '123 Calle Principal', '2015-01-15', 60000.00, 500.00, 1),
('002', 'Jane',    'Smith', 'Pediatra', '456 Calle Elm', '2023-02-20', 75000.00, NULL, 2),
('003', 'Michael', 'Johnson', 'Odontologia', '789 Calle Oak', '2023-03-10', 55000.00, 3000.00, 3),
('004', 'Sarah',   'Williams', 'Geriatria', '101 Calle Pine', '2016-04-05', 65000.00, 4000.00, 4),
('005', 'Robert',  'Lee', 'Medicina General', '222 Calle Cedar', '2023-05-18', 70000.00, NULL, 5),
('006', 'Emily',   'Brown', 'Pediatria', '333 Calle Birch', '2023-06-30', 80000.00, 6000.00, 6),
('007', 'Daniel',  'Taylor', 'Odontologia', '444 Calle Maple', '2023-07-22', 60000.00, 2500.00, 7),
('008', 'Olivia',  'Clark', 'Medicina General', '555 Calle Walnut', '2015-08-08', 85000.00, NULL, 8),
('009', 'William', 'Lewis', 'Geriatria', '666 Calle Cherry', '2023-09-12', 75000.00, 5000.00, 9),
('010', 'Ava',     'Martinez', 'Cirugia', '777 Calle Spruce', '2018-10-01', 55000.00, 3500.00, 10);


INSERT INTO ENFERMO (ENF_INSCRIPCION, ENF_NOMBRE, ENF_APELLIDO, ENF_DIRECCION, ENF_FECHANAC, ENF_ESTADOSALUD, ENF_NSS) VALUES
('E01', 'Juan', 'P�rez', '123 Calle Salud', '2013-05-15', 'Buena', '123-45-6789'),
('E02', 'Laura', 'G�mez', '456 Calle Bienestar', '2015-08-20', 'Regular', '987-65-4321'),
('E03', 'Miguel', 'L�pez', '789 Avenida Vitalidad', '2012-03-10', 'Excelente', '555-11-2233'),
('E04', 'Isabel', 'Rodr�guez', '101 Camino de Recuperaci�n', '2013-07-05', 'Buena', '777-22-9999'),
('E05', 'Carlos', 'Mart�nez', '222 Bulevar de Salud', '2014-06-18', 'Regular', '111-44-7777'),
('E06', 'Ana', 'Fern�ndez', '333 Calle Cuidado', '2015-12-30', 'Mala', '888-33-1111'),
('E07', 'Sergio', 'D�az', '444 Calle Sanaci�n', '2016-07-22', 'Buena', '666-55-4444'),
('E08', 'Elena', 'S�nchez', '555 Avenida Bienestar', '2013-08-08', 'Excelente', '222-99-8888'),
('E09', 'Pedro', 'Gonz�lez', '666 Calle Recuperaci�n', '1998-09-12', 'Mala', '444-77-5555'),
('E10', 'Mar�a', 'Torres', '777 Bulevar de Vitalidad', '1980-10-01', 'Regular', '333-88-2222');

SELECT * FROM DPTO
SELECT * FROM EMP
SELECT * FROM ENFERMO

---------------------------------------------------------------------------------
/*  PROCEDIMIENTO PARA OBTENER EMPLEADOS QUE SE DIERON DE ALTA ANTES DEL 2018  */
SELECT *
FROM EMP
WHERE YEAR(EMP_FECHAALT) < 2018 ;

---------------------------------------------------------
/*  PROCEDIMIENTO PARA INSERTAR UN NUEVO DEPARTAMENTO  */
CREATE PROCEDURE InsertarDepartamento
    @DPTO_NO INT,
    @DPTO_NOMBRE VARCHAR(50),
    @DPTO_LOC VARCHAR(50)
AS
BEGIN
    BEGIN TRY
        INSERT INTO DPTO (DPTO_NO, DPTO_NOMBRE, DPTO_LOC)
        VALUES (@DPTO_NO, @DPTO_NOMBRE, @DPTO_LOC);
        PRINT 'Departamento insertado con �xito.';
    END TRY
    BEGIN CATCH
        PRINT 'Error al insertar el departamento.';
    END CATCH
END;
-- INSERTAMOS EL DEPARTAMENTO
EXEC InsertarDepartamento @DPTO_NO = 17, @DPTO_NOMBRE = 'PERU', @DPTO_LOC = 'Lima';
-- VEMOS EL DATO INSERTADO
SELECT *
 FROM DPTO
WHERE DPTO_NO = 17;

-----------------------------------------------------------------
/* PROCEDIMIENTO PARA RECUPERAR EDAD PROMEDIO DE LAS EMPLEADOS */
CREATE PROCEDURE ObtenerPromedioEdadEnfermos
AS
BEGIN
    SELECT AVG(DATEDIFF(YEAR, ENF_FECHANAC, GETDATE())) AS PromedioEdadEnfermos
    FROM ENFERMO;
END;
-- EJECUTAMOS EL PROCEDIMIENTO
EXEC ObtenerPromedioEdadEnfermos;

------------------------------------------------------------------------------------------------------------------------
/* CREAR UN PROCEDIMIENTO PARA DEVOLVER EL APELLIDO, OFICIO Y SALARIO, PAS�NDOLECOMO PAR�METRO EL N�MERO DEL EMPLEADO */
CREATE PROCEDURE ObtenerInfoEmpleado
    @EmpleadoNo CHAR(3)
AS
BEGIN
    SELECT EMP_APE AS Apellido, EMP_OFI AS Oficio, EMP_SALARIO AS Salario
    FROM EMP
    WHERE EMP_COD = @EmpleadoNo;
END;
-- EJECUTAMOS EL PROCEDIMINETO
EXEC ObtenerInfoEmpleado @EmpleadoNo = '001';

-----------------------------------------------------------------------------------------------------------
/* CREAR UN PROCEDIMIENTO ALMACENADO PARA DAR DE BAJA A UN EMPLEADO PAS�NDOLECOMO PAR�METRO SU APELLIDO. */
CREATE PROCEDURE DarDeBajaEmpleado
    @Apellido VARCHAR(50)
AS
BEGIN
    DELETE FROM EMP
    WHERE EMP_APE = @Apellido;
END;
-- EJECUTAMOS Y ELIMINAMOS AL EMPLEADO SMITH
EXEC DarDeBajaEmpleado @Apellido = 'Smith';
SELECT * FROM EMP

-------------------------------------------------------------------------------------------------------------------------------
/* SE CREA PROCEDIMIENTOS ALMACENADOS PARA INGRESAR, CONSULTAR, MODIFICAR Y ELIMINAR DATOS DE LA BASE, JUNTO CON FUNCIONES.  */

  -- ********************************************************************
  -- 1 // Procedimiento almacenado para insertar datos en la tabla 'EMP':
CREATE PROCEDURE InsertarEmpleado
    @EMP_COD CHAR(3),
    @EMP_NOM VARCHAR(50),
    @EMP_APE VARCHAR(50),
    @EMP_OFI VARCHAR(40),
    @EMP_DIRE VARCHAR(50),
    @EMP_FECHAALT DATE,
    @EMP_SALARIO MONEY,
    @EMP_COMISION MONEY,
    @DPTO_NO INT
AS
BEGIN
    INSERT INTO EMP (EMP_COD, EMP_NOM, EMP_APE, EMP_OFI, EMP_DIRE, EMP_FECHAALT, EMP_SALARIO, EMP_COMISION, DPTO_NO)
    VALUES (@EMP_COD, @EMP_NOM, @EMP_APE, @EMP_OFI, @EMP_DIRE, @EMP_FECHAALT, @EMP_SALARIO, @EMP_COMISION, @DPTO_NO);
END;
  -- EJECUTAMOS EL PROCEDIMIENTO
EXEC InsertarEmpleado
    @EMP_COD = '012',
    @EMP_NOM = 'Mar�a',
    @EMP_APE = 'L�pez',
    @EMP_OFI = 'Odontologia',
    @EMP_DIRE = '111 Calle Mar',
    @EMP_FECHAALT = '2023-11-04',
    @EMP_SALARIO = 55000.00,
    @EMP_COMISION = 475.00,
    @DPTO_NO = 2;
  -- VEMOS EMPLEADO INSERTADO
  SELECT *
	FROM EMP
	WHERE EMP_COD = '012';

  -- ****************************************************************************************************
  -- 2 // Procedimiento almacenado para consultar datos de la tabla 'EMP' basado en el n�mero de empleado
CREATE PROCEDURE ConsultarEmpleadoPorNumero
    @EMP_COD CHAR(3)
AS
BEGIN
    SELECT *
    FROM EMP
    WHERE EMP_COD = @EMP_COD;
END;
  -- EJECUTAMOS PROCEDIMIENTO
EXEC ConsultarEmpleadoPorNumero @EMP_COD = '007';

  -- ***********************************************************************************
  -- 3 // Procedimiento almacenado para modificar datos de un empleado en la tabla 'EMP'
CREATE PROCEDURE ModificarEmpleado
    @EMP_COD CHAR(3),
    @Nuevo_EMP_NOM VARCHAR(50),
    @Nuevo_EMP_APE VARCHAR(50),
    @Nuevo_EMP_OFI VARCHAR(40),
    @Nuevo_EMP_DIRE VARCHAR(50),
    @Nuevo_EMP_FECHAALT DATE,
    @Nuevo_EMP_SALARIO MONEY,
    @Nuevo_EMP_COMISION MONEY,
    @Nuevo_DPTO_NO INT
AS
BEGIN
    UPDATE EMP
    SET EMP_NOM = @Nuevo_EMP_NOM,
        EMP_APE = @Nuevo_EMP_APE,
        EMP_OFI = @Nuevo_EMP_OFI,
        EMP_DIRE = @Nuevo_EMP_DIRE,
        EMP_FECHAALT = @Nuevo_EMP_FECHAALT,
        EMP_SALARIO = @Nuevo_EMP_SALARIO,
        EMP_COMISION = @Nuevo_EMP_COMISION,
        DPTO_NO = @Nuevo_DPTO_NO
    WHERE EMP_COD = @EMP_COD;
END;
  -- EJECUTAMOS EL PROCEDIMIENTO
  -- VEMOS EL EMP QUE VAMOS A MODIFICAR
  SELECT *
	FROM EMP
	WHERE EMP_COD = '012';
  -- MODIFICAMOS LOS DATOS
EXEC ModificarEmpleado
    @EMP_COD = '012', 
    @Nuevo_EMP_NOM = 'MARIA',
    @Nuevo_EMP_APE = 'CARRILLO',
    @Nuevo_EMP_OFI = 'PEDIATRA',
    @Nuevo_EMP_DIRE = '124 AV SOL',
    @Nuevo_EMP_FECHAALT = '2023-11-04', 
    @Nuevo_EMP_SALARIO = 5400.00, 
    @Nuevo_EMP_COMISION = 700.00, 
    @Nuevo_DPTO_NO = 7; 
  -- VOLVEMOS A EJECUTAR ESTE COMANDO PARA VERIFICAR EL CAMBIO
  SELECT *
	FROM EMP
	WHERE EMP_COD = '012';

  -- ************************************************************************************************
  -- 4 // Procedimiento almacenado para eliminar un empleado de la tabla 'EMP' por n�mero de empleado
CREATE PROCEDURE EliminarEmpleadoPorNumero
    @EMP_COD CHAR(3)
AS
BEGIN
    DELETE FROM EMP
    WHERE EMP_COD = @EMP_COD;
END;

 -- EJECUTAMOS EL PROCEDIMIENTO
 EXEC EliminarEmpleadoPorNumero @EMP_COD = '005';
 -- VERIFICAMOS LA ELIMINACION
 SELECT * FROM EMP

/* CONSULTAS REQUERIDAS EN TRFINAL ENTREGABLE 2 CULMINADO */
/* ======================================================*/




-- |=============================|
-- | GIOVANNI ELNICH NAYHUA LIMA |
-- |=============================|

--*********************************
-- Creaci�n de la base de datos
CREATE DATABASE ClinicaBuenaSalud;

USE ClinicaBuenaSalud;

-- Tabla para empleados
CREATE TABLE Empleados (
    NumeroEmpleado INT PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    FechaAlta DATE,
    FechaNacimiento DATE,
    Departamento VARCHAR(50),
    Oficio VARCHAR(50),
    Salario DECIMAL(10, 2)
);

-- Tabla para departamentos
CREATE TABLE Departamentos (
    IDDepartamento INT PRIMARY KEY IDENTITY(1, 1),
    NombreDepartamento VARCHAR(50)
);

-- Tabla para pacientes
CREATE TABLE Pacientes (
    NumeroPaciente INT PRIMARY KEY,
    Nombre VARCHAR(50),
    Apellido VARCHAR(50),
    FechaNacimiento DATE,
    Genero VARCHAR(10),
    Direccion VARCHAR(100),
    Telefono VARCHAR(15),
    Email VARCHAR(100)
);

-- Tabla para consultas m�dicas
CREATE TABLE ConsultasMedicas (
    NumeroConsulta INT PRIMARY KEY,
    FechaConsulta DATE,
    NumeroPaciente INT,
    NumeroEmpleado INT,
    Diagnostico VARCHAR(200),
    FOREIGN KEY (NumeroPaciente) REFERENCES Pacientes (NumeroPaciente),
    FOREIGN KEY (NumeroEmpleado) REFERENCES Empleados (NumeroEmpleado)
);

-- Tabla para procedimientos m�dicos
CREATE TABLE ProcedimientosMedicos (
    IDProcedimiento INT PRIMARY KEY IDENTITY(1, 1),
    NombreProcedimiento VARCHAR(100),
    Costo DECIMAL(10, 2)
);

-- Tabla de detalles de procedimientos m�dicos para consultas
CREATE TABLE DetallesProcedimientos (
    IDDetalle INT PRIMARY KEY IDENTITY(1, 1),
    NumeroConsulta INT,
    IDProcedimiento INT,
    Cantidad INT,
    FOREIGN KEY (NumeroConsulta) REFERENCES ConsultasMedicas (NumeroConsulta),
    FOREIGN KEY (IDProcedimiento) REFERENCES ProcedimientosMedicos (IDProcedimiento)
);

---------------------------------------------------------------------------------------------
--							INSERTAMOS DATOS EN LAS TABLAS   
---------------------------------------------------------------------------------------------
-- Insertar datos de empleados
INSERT INTO Empleados (NumeroEmpleado, Nombre, Apellido, FechaAlta, FechaNacimiento, Departamento, Oficio, Salario)
VALUES
(1, 'Juan', 'Perez', '2020-05-15', '1980-02-20', 'DepartamentoX', 'M�dico', 75000.00),
(2, 'Ana', 'Gomez', '2019-09-10', '1985-07-12', 'DepartamentoY', 'Enfermera', 45000.00);

INSERT INTO Empleados (NumeroEmpleado, Nombre, Apellido, FechaAlta, FechaNacimiento, Departamento, Oficio, Salario)
VALUES
(3, 'Carlos', 'L�pez', '2017-03-20', '1982-11-05', 'DepartamentoY', 'M�dico', 80000.00),
(4, 'Mar�a', 'Rodr�guez', '2016-06-10', '1978-04-15', 'DepartamentoY', 'Enfermera', 50000.00);

INSERT INTO Empleados (NumeroEmpleado, Nombre, Apellido, FechaAlta, FechaNacimiento, Departamento, Oficio, Salario)
VALUES
(5, 'Laura', 'Diaz', '2017-08-12', '1986-04-28', 'DepartamentoX', 'Enfermera', 48000.00),
(6, 'Carlos', 'S�nchez', '2016-11-22', '1981-12-15', 'DepartamentoX', 'M�dico', 72000.00);

-- Insertar datos de departamentos
INSERT INTO Departamentos (NombreDepartamento)
VALUES
('DepartamentoX'),
('DepartamentoY');

-- Insertar datos de pacientes
INSERT INTO Pacientes (NumeroPaciente, Nombre, Apellido, FechaNacimiento, Genero, Direccion, Telefono, Email)
VALUES
(1, 'Mar�a', 'L�pez', '1990-03-25', 'Femenino', 'Calle 123', '555-123-4567', 'maria@email.com'),
(2, 'Carlos', 'Ram�rez', '1987-07-10', 'Masculino', 'Avenida 456', '555-789-1234', 'carlos@email.com');

-- Insertar datos de consultas m�dicas
INSERT INTO ConsultasMedicas (NumeroConsulta, FechaConsulta, NumeroPaciente, NumeroEmpleado, Diagnostico)
VALUES
(1, '2023-10-15', 1, 1, 'Gripe com�n'),
(2, '2023-10-16', 2, 2, 'Examen de rutina');

-- Insertar datos de procedimientos m�dicos
INSERT INTO ProcedimientosMedicos (NombreProcedimiento, Costo)
VALUES
('Examen de sangre', 100.00),
('Radiograf�a', 200.00);

-- Insertar detalles de procedimientos
INSERT INTO DetallesProcedimientos (NumeroConsulta, IDProcedimiento, Cantidad)
VALUES
(1, 1, 1),
(2, 2, 1);

-- SELECCIONAMOS LAS TABLAS PARA VERIFICAR LA CREACION Y EL INGRESO CORRECTO DE DATOS
SELECT * FROM Empleados;
SELECT * FROM Departamentos;
SELECT * FROM Pacientes;
SELECT * FROM ConsultasMedicas;
SELECT * FROM ProcedimientosMedicos;
SELECT * FROM DetallesProcedimientos;
-------------------------------------------------------------------------------------------------------------


-- ******************************************************************************************
-- 1. Consulta para obtener todos los empleados que se dieron de alta antes del a�o 2018 y 
-- que pertenecen a un departamento espec�fico
SELECT * 
FROM Empleados
WHERE FechaAlta < '2018-01-01' AND Departamento = 'DepartamentoX';

SELECT * 
FROM Empleados
WHERE FechaAlta < '2018-01-01' AND Departamento = 'DepartamentoY';

-- ******************************************************************************************

-- 2. Consulta para insertar un nuevo departamento:
-- Crear procedimiento para insertar un nuevo departamento
CREATE PROCEDURE InsertarDepartamento
    @NombreDepartamento VARCHAR(50)
AS
BEGIN
    INSERT INTO Departamentos (NombreDepartamento)
    VALUES (@NombreDepartamento);
    PRINT 'Departamento insertado correctamente.';
END;

-- AHORA INSERTAMOS UN PROCEDIMIENTO
EXEC InsertarDepartamento @NombreDepartamento = 'DepartamentoZ';

-- VEMOS QUE EL DEPARTAMENTO SE CREO CON EXITO
SELECT * FROM Departamentos;

-- INSERTAMOS DATOS QUE INCLUYAN ESTE NUEVO DEPARTAMENTO
INSERT INTO Empleados (NumeroEmpleado, Nombre, Apellido, FechaAlta, FechaNacimiento, Departamento, Oficio, Salario)
VALUES
(7, 'Rudy', 'Gonz�lez', '2020-01-10', '1986-08-30', 'DepartamentoZ', 'Enfermero', 47000.00),
(8, 'Mateo', 'L�pez', '2018-12-05', '1987-05-12', 'DepartamentoZ', 'M�dico', 79000.00);

-- SELECCIONAMOS SOLO LOS EMPLEADOS DEL DEPARTAMENTOZ
SELECT *
FROM Empleados
WHERE Departamento = 'DepartamentoZ';

-- ******************************************************************************************
-- 3. Consulta para calcular el promedio de edad de las personas por cada departamento:
-- Crear procedimiento para calcular el promedio de edad de personas por departamento
CREATE PROCEDURE CalcularPromedioEdadPorDepartamento
AS
BEGIN
    SELECT Departamento, AVG(DATEDIFF(YEAR, FechaNacimiento, GETDATE())) AS PromedioEdad
    FROM Empleados
    GROUP BY Departamento;
END;

-- EJECUTAMOS PARA VER LA CREACION DEL PROCEDIMIENTO
EXEC CalcularPromedioEdadPorDepartamento;

-- ******************************************************************************************
-- 4. Consulta para obtener el apellido, oficio y salario de un empleado por n�mero de empleado:
-- Crear procedimiento para obtener informaci�n de un empleado por n�mero de empleado
CREATE PROCEDURE ObtenerInfoEmpleado
    @NumeroEmpleado INT
AS
BEGIN
    SELECT Apellido, Oficio, Salario
    FROM Empleados
    WHERE NumeroEmpleado = @NumeroEmpleado;
END;

-- Ejecutamos el procedimiento para obtener informaci�n de un empleado
EXEC ObtenerInfoEmpleado @NumeroEmpleado = 1;
EXEC ObtenerInfoEmpleado @NumeroEmpleado = 7;

-- ******************************************************************************************
-- 5. Consulta para dar de baja a un empleado por apellido:
CREATE PROCEDURE DarDeBajaEmpleado
    @Apellido VARCHAR(50)
AS
BEGIN
    DELETE FROM Empleados
    WHERE Apellido = @Apellido;
END;

-- INSERTAMOS DATOS PARA PROBAR QUE FUNCIONE CORRECTAMENTE LA FUNCION
INSERT INTO Empleados (NumeroEmpleado, Nombre, Apellido, FechaAlta, FechaNacimiento, Departamento, Oficio, Salario)
VALUES
(9, 'Elena', 'Ram�rez', '2022-03-15', '1990-05-10', 'DepartamentoX', 'M�dico', 76000.00);

-- VEMOS LA INSERCION DE DATOS EN LA TABLA EMPLEADO
SELECT * FROM Empleados

-- Ejecutar el procedimiento para dar de baja a un empleado
EXEC DarDeBajaEmpleado @Apellido = 'Ram�rez';

-- VERIFICAMOS LA ELIMINACION DEL EMPLEADO
SELECT * FROM Empleados

-- ******************************************************************************************
-- 6. Consulta para recuperar el promedio de salario de empleados por departamento:
SELECT Departamento, AVG(Salario) AS SalarioPromedio
FROM Empleados
GROUP BY Departamento;

-- ******************************************************************************************
-- 7. Consulta para obtener la lista de pacientes de un m�dico espec�fico:
SELECT P.Nombre, P.Apellido
FROM Pacientes P
JOIN ConsultasMedicas C ON P.NumeroPaciente = C.NumeroPaciente
JOIN Empleados E ON C.NumeroEmpleado = E.NumeroEmpleado
WHERE E.Nombre = 'Juan' AND E.Apellido = 'Perez';

SELECT P.Nombre, P.Apellido
FROM Pacientes P
JOIN ConsultasMedicas C ON P.NumeroPaciente = C.NumeroPaciente
JOIN Empleados E ON C.NumeroEmpleado = E.NumeroEmpleado
WHERE E.Nombre = 'Ana' AND E.Apellido = 'Gomez';

-- ******************************************************************************************
-- 8. Consulta para obtener el detalle de procedimientos realizados en una consulta m�dica espec�fica:
DECLARE @NumeroConsultaEspecifica INT;
SET @NumeroConsultaEspecifica = 1001; -- Reemplaza con el valor espec�fico que deseas

SELECT DM.Cantidad, PM.NombreProcedimiento, PM.Costo
FROM DetallesProcedimientos DM
JOIN ProcedimientosMedicos PM ON DM.IDProcedimiento = PM.IDProcedimiento
WHERE DM.NumeroConsulta = 2;

-- ******************************************************************************************
-- **************** 3 ***************

--A-  CREAMOS EL PROCEDIMIENTO PARA INGRESAR AL PACIENTE
CREATE PROCEDURE IngresarPaciente
	@NumeroPaciente INT,
    @Nombre NVARCHAR(100),
    @Apellido NVARCHAR(100),
    @FechaNacimiento DATE,
    @Genero CHAR(1),
    @Direccion NVARCHAR(255),
    @Telefono NVARCHAR(15),
    @Email NVARCHAR(100)
AS
BEGIN
    INSERT INTO Pacientes (Nombre, Apellido, FechaNacimiento, Genero, Direccion, Telefono, Email)
    VALUES (@NumeroPaciente, @Nombre, @Apellido, @FechaNacimiento, @Genero, @Direccion, @Telefono, @Email);
END;

-- PONEMOS EL VALOR NUMEROPACIENTO COMO AUTOINCREMENTO
ALTER TABLE Pacientes
ALTER COLUMN NumeroPaciente INT IDENTITY(1,1);

-- INGRESAMOS LOS DATOS DEL PACIENTE
-- Ejemplo de c�mo ingresar un nuevo paciente utilizando el procedimiento almacenado
EXEC IngresarPaciente
	@NumeroPaciente= '3',
    @Nombre = 'Cristiano',
    @Apellido = 'Ronaldo',
    @FechaNacimiento = '2005-07-15',
    @Genero = 'M',
    @Direccion = 'Av. La Mar',
    @Telefono = '457-524-7890',
    @Email = 'crisronaldo@gmail.com';

	select * from Pacientes

